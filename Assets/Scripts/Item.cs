using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public bool fuel = false;
    public bool furnace = false;
    public bool heating = false;
    public bool workingBench = false;
    public bool workingMachine = false;
    public bool anvil = false;
    public int type = 0;

    public bool pickable = true;
    public GameObject nextitem = null;
    public Material bucketMaterial;
    public int alloyId = 0;

    public void Empty() {
        Debug.Log("Cubo vacio");
        if(nextitem != null) nextitem = null;
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
    }

    public void Full() {
        if (nextitem == null) {
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
            gameObject.transform.GetChild(0).gameObject.GetComponent<Renderer>().material = bucketMaterial;
        }
    }
}
