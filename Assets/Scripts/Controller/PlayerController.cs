using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public int player;
    public float speed = 2.0f;
    public Rigidbody rb;
    public Animator anim;
    public float throwForce = 300;

    public int pickaxeHealth = 5;
    public int hammerHealth = 5;

    public bool freezing = false;
    public GameObject onHand = null;
    public bool usingTable = false;
    public bool occupied = false;
    public bool onRing = false;
    
    public bool dashing = false;
    public float dashCd = 0;
    public float dashT = 0.2f;
    public float dashDelay = 1;

    
    public Vector3 inputAxis;
    public Vector3 mov;
    public LayerMask sMask;
    public LayerMask iMask;

    public float raycastOffset;

    [HideInInspector]
    public bool viewUse = false;

    Vector3 dir, lastMov, forward;
    [HideInInspector]
    public GameObject selection, tempSelection, itemSelected = null;
    public GameObject holdItem;

    PlayerInput input;

    
    void Start()
    {
        rb = this.gameObject.GetComponent<Rigidbody>();
        anim = this.gameObject.GetComponent<Animator>();
        input = this.gameObject.GetComponent<PlayerInput>();
        //if(this.gameObject.GetComponent<PlayerInput>().actions == null && pInput != null)
          //  this.gameObject.GetComponent<PlayerInput>().actions = pInput;
    }

    void FixedUpdate()
    {
        if(dashCd < dashDelay + 0.1f)
            dashCd += Time.deltaTime;
        Movement();
        Detect();
    }

    void Movement()
    {
        mov = freezing ? Vector3.zero : inputAxis;

        Vector3 yAxis = new Vector3(0, rb.velocity.y, 0);
        if (!dashing)
            rb.velocity = (mov * speed) + yAxis;

        if (mov == Vector3.zero)
            forward = lastMov;
        else
            forward = mov;

        forward.y = 0.0f;
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(forward), 15f * Time.deltaTime);
        lastMov = forward;
    }

    void Detect() {
        RaycastHit hit1;
        RaycastHit hit2;
        Debug.DrawRay(transform.position - transform.up * raycastOffset + transform.right * 0.5f, transform.forward * 1.5f, Color.red);
        Debug.DrawRay(transform.position - transform.up * raycastOffset - transform.right * 0.5f, transform.forward * 1.5f, Color.red);
        bool leftRay = Physics.Raycast(transform.position - transform.up * raycastOffset + transform.right * 0.5f, transform.forward, out hit1, 1.5f, sMask);
        bool rightRay = Physics.Raycast(transform.position - transform.up * raycastOffset - transform.right * 0.5f, transform.forward, out hit2, 1.5f, sMask);

        //Debug.DrawRay(transform.position - (transform.up * raycastOffset), transform.forward * 2.5f, Color.red);
        if(leftRay && rightRay) {
            GameObject sel1 = hit1.transform.gameObject;
            GameObject sel2 = hit2.transform.gameObject;
            if(sel1 != sel2) {
                tempSelection = ClosestSurface(sel1, sel2);
                selectRaycast();
            } else {
                tempSelection = sel1;
                selectRaycast();
            }
        } else if(leftRay) {
            tempSelection = hit1.transform.gameObject;
            selectRaycast();
        } else if(rightRay) {
            tempSelection = hit2.transform.gameObject;
            selectRaycast();
        } else {
            selection = null;
            if(tempSelection != selection) {
                tempSelection.GetComponent<Surface>().selected = false;
                tempSelection = selection;
            }
        }

        if(Physics.Raycast(transform.position - (transform.up * raycastOffset), transform.forward, out hit1, 2.5f, iMask)) {
            itemSelected = hit1.transform.gameObject;
        } else
            itemSelected = null;
    }

    void selectRaycast() {
        if(tempSelection != selection) {
            if(selection != null)
                selection.GetComponent<Surface>().selected = false;
        }

        selection = tempSelection;
        selection.GetComponent<Surface>().selected = true;
    }

    GameObject ClosestSurface(GameObject surface1, GameObject surface2) {
        Vector3 currentPos = this.transform.position;
        Vector3 dir1 = surface1.transform.position - currentPos;
        Vector3 dir2 = surface2.transform.position - currentPos;

        float sqr1 = dir1.sqrMagnitude;
        float sqr2 = dir2.sqrMagnitude;

        if(sqr1 > sqr2) {
            return surface2;
        } else {
            return surface1;
        }
    }

    void OnMovement(InputValue value)
    {
        Vector2 m = value.Get<Vector2>();
        inputAxis = new Vector3(m.x, 0, m.y);

        if (onRing && selection != null) {
            if (input.currentControlScheme == "Keyboard") {
                if (m.x == 1) {
                    selection.GetComponent<Anvil>().ring.RightCake();
                } else if (m.x == -1) {
                    selection.GetComponent<Anvil>().ring.LeftCake();
                }
            } else if (input.currentControlScheme == "Gamepad") {
                selection.GetComponent<Anvil>().ring.getGamepadAxis(m);
            }
        }
    }
    void OnThrow()
    {
        if (onHand != null) {
            if (onHand.tag == "Bucket" && onHand.GetComponent<Item>().nextitem != null) {
                return;
            } else {
                onHand.transform.rotation = this.transform.rotation;
                onHand.GetComponent<Throw>().enabled = true;
                onHand.layer = 12;
                onHand.GetComponent<Rigidbody>().isKinematic = false;
                onHand.GetComponent<Collider>().enabled = true;
                onHand.transform.parent = null;
                onHand.GetComponent<Rigidbody>().AddForce(transform.forward * throwForce*2);
                onHand.GetComponent<Rigidbody>().AddForce(Vector3.up * throwForce/2);
                onHand = null;
            }
        }
    }
    void OnDash()
    {
        if (dashCd >= dashDelay) { 
        dashing = true;
        rb.AddForce(transform.forward * 10, ForceMode.Impulse);
        if (dashing)
            StartCoroutine(dashTime());
        }
    }
    IEnumerator dashTime()
    {
        yield return new WaitForSeconds(dashT);
        dashCd = 0;
        dashing = false;
    }
    void OnInteract() {
        //COGER objeto en el suelo
        if(itemSelected != null && onHand == null) {
            if (itemSelected.GetComponent<Item>().pickable) {
                itemSelected.layer = 0;
                onHand = itemSelected;
                itemSelected = null;
                onHand.GetComponent<Rigidbody>().isKinematic = true;
                onHand.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                onHand.GetComponent<Collider>().enabled = false;
                onHand.transform.position = holdItem.transform.position;
                onHand.transform.parent = holdItem.transform;
            }
        }
        //COGER objeto encima de superficie
        else if(selection != null && onHand == null) {
            if (selection.GetComponent<CraftingTable>() != null) {
                selection.GetComponent<Surface>().PickOnSurface(this);
            }
            if (!selection.GetComponent<Surface>().usable)
                selection.GetComponent<Surface>().PickOnSurface(this);
        }
        //DEJAR objeto en el suelo
        else if(onHand != null && selection == null) {
            onHand.layer = 12;
            onHand.GetComponent<Rigidbody>().isKinematic = false;
            onHand.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            onHand.GetComponent<Collider>().enabled = true;
            onHand.transform.parent = null;
            onHand = null;

        }
        //DEJAR objeto encima de superficie (y CUBOS)
        else if(onHand != null && selection != null) {
            if (onHand.tag == "Bucket" ) {
                //COGER metal fundido del horno
                if (selection.GetComponent<Furnace>() != null) {
                    selection.GetComponent<Surface>().PickOnSurface(this);
                } //DEJAR metal en el yunque
                else if (selection.GetComponent<Anvil>() != null && onHand.GetComponent<Item>().nextitem != null) {
                    if(!selection.GetComponent<Surface>().occupied) {
                        GameObject metal = Instantiate(onHand.GetComponent<Item>().nextitem);
                        onHand.GetComponent<Item>().nextitem = null;
                        onHand.GetComponent<Item>().Empty();
                        selection.GetComponent<Surface>().LeaveInSurface(metal);
                    }
                } else if(!selection.GetComponent<Surface>().occupied) {
                    selection.GetComponent<Surface>().LeaveInSurface(onHand);
                }
            } else if(!selection.GetComponent<Surface>().occupied) {
                selection.GetComponent<Surface>().LeaveInSurface(onHand);
            }
        }
    }

    void OnUse() {
        viewUse = true;
        if(selection != null && onHand == null && !usingTable) {
            if (selection.GetComponent<Surface>().usable) {
                selection.GetComponent<Surface>().PickOnSurface(this);
            }
        }
    }

    void OnDeUse() {
        viewUse = false;
        if(selection != null && onHand == null && usingTable) {
            if(selection.GetComponent<Surface>().usable) {
                selection.GetComponent<Surface>().beingUsed = false;
            }
            freezing = false;
            usingTable = false;
        }
    }

    void OnSpecial() {
        if(selection != null && onHand == null && !usingTable) {
            selection.GetComponent<Surface>().Special(this);
        }
    }
}


