using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throw : MonoBehaviour
{
    public LayerMask mask1;
    public LayerMask mask2;
    float t = 0;

    private void OnEnable()
    {
        t = 0;
    }
    private void FixedUpdate()
    {
        if (t < 1f)
            t += Time.deltaTime;
        
        RaycastHit hit1;
        RaycastHit hit2;
        Debug.DrawRay(transform.position, -transform.up * 1f, Color.green);
        Debug.DrawRay(transform.position, transform.forward * 0.5f, Color.blue);
        if (Physics.Raycast(transform.position, -transform.up, out hit1, 1f,  mask1))
        {
            if(t > 0.4f)
            {
                hit1.transform.gameObject.GetComponent<Surface>().LeaveInSurface(this.gameObject);
                this.enabled = false;
            }
        }

        if (Physics.Raycast(transform.position, transform.forward, out hit2, 0.5f, mask2))
        {
            PlayerController pController = hit2.transform.gameObject.GetComponent<PlayerController>();
            pController.onHand = this.gameObject;
            pController.onHand.GetComponent<Rigidbody>().isKinematic = true;
            pController.onHand.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            pController.onHand.GetComponent<Collider>().enabled = false;
            pController.onHand.transform.position = pController.holdItem.transform.position;
            pController.onHand.transform.parent = pController.holdItem.transform;
            this.enabled = false;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
            this.enabled = false;
    }

}
