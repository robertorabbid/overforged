using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Furnace : Surface
{
    public float fuel = 0f;
    public float maxFuel = 50f;
    public float fuelPerItem = 5f;
    public bool infFuel = false;

    public float timer = 0;
    public float furnaceTime = 2.0f;
    public GameObject bar;
    public GameObject fuelBar;
    public GameObject alloy = null;

    public bool furnaced = false;

    public override void Update() {
        base.Update();
        if (infFuel) fuelBar.transform.gameObject.SetActive(false);

        if (beingUsed) {
            bar.transform.gameObject.SetActive(true);
            bar.transform.GetChild(1).GetComponent<Image>().fillAmount = timer / furnaceTime;
            if(fuel >= 0.0f || infFuel) {
                fuel -= Time.deltaTime;
                timer += Time.deltaTime;

                if (timer >= furnaceTime) {
                    furnaced = true;
                    beingUsed = false;
                }
            }
        }

        fuelBar.transform.GetChild(1).GetComponent<Image>().fillAmount = fuel / maxFuel * 0.75f;
    }

    public override void LeaveInSurface(GameObject item) {
        //base.LeaveInSurface(item);
        if (item.GetComponent<Item>().fuel) {
            fuel += fuelPerItem;
            if(fuel >= maxFuel) fuel = maxFuel;
            if(item.transform.parent != null)
                item.transform.parent.parent.GetComponent<PlayerController>().onHand = null;
            GameObject.Destroy(item);
        } else if (item.GetComponent<Item>().furnace) {
            if (onSurface == null) {
                beingUsed = true;
                onSurface = item.GetComponent<Item>().nextitem;
                if(item.transform.parent != null)
                    item.transform.parent.parent.GetComponent<PlayerController>().onHand = null;
                GameObject.Destroy(item);
            } else {
                if (item.GetComponent<Item>().alloyId == onSurface.GetComponent<Item>().alloyId && item.GetComponent<Item>().nextitem != onSurface) {
                    Debug.Log("Aleacion");
                }
            }
        }
    }

    public override void PickOnSurface(PlayerController player) {
        if (furnaced && player.onHand != null) {
            if (player.onHand.tag == "Bucket" && player.onHand.GetComponent<Item>().nextitem == null) {
                player.onHand.GetComponent<Item>().bucketMaterial = onSurface.GetComponent<Item>().bucketMaterial;
                player.onHand.GetComponent<Item>().Full();
                player.onHand.GetComponent<Item>().nextitem = onSurface;
                onSurface = null;
                furnaced = false;
                timer = 0;
                bar.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
                bar.transform.gameObject.SetActive(false);
            }
        }
        //base.PickOnSurface(player);
    }
}
