using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingTable : Surface
{
    public float timer = 0;
    public float craftingTime = 4.0f;
    public bool crafted = false;
    public GameObject bar;
    public int type;
    public CraftingTable otherT;

    PlayerController pl;
    bool correct = false;

    public override void Update() {
        base.Update();

        if(beingUsed)
            otherT.usable = false;
        else
            if(correct) otherT.usable = true;

        if (beingUsed && usable) {
            bar.transform.gameObject.SetActive(true);
            bar.transform.GetChild(1).GetComponent<Image>().fillAmount = timer / craftingTime;
            timer += Time.deltaTime;
            if (timer >= craftingTime) {
                crafted = true;
                CreateItem();
            }
        } else {
            if (!otherT.beingUsed || !otherT.usable) {
                bar.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
                bar.transform.gameObject.SetActive(false);
            }
            timer = 0;
        }

        if (onSurface == null) type = 100;
    }

    public override void LeaveInSurface(GameObject item) {
        if (item.GetComponent<Item>().workingBench && !item.GetComponent<Item>().heating) {
            type = item.GetComponent<Item>().type;
            base.LeaveInSurface(item);
            CreateItem();
        }
    }

    public override void PickOnSurface(PlayerController player) {
        if (player.viewUse) {
            if(player != pl)
                pl = player;
            beingUsed = true;
            player.usingTable = true;
            player.freezing = true;
        } else {
            base.PickOnSurface(player);
            CreateItem();
        }
    }

    public void CreateItem() {
        if(onSurface == null) type = 100;
        // 0 = madera 100 = vacio
        if ((type == 0 && otherT.type == 100) || (type == 100 && otherT.type == 0)) {
            usable = true;
            correct = true;
            if (crafted) {
                GameObject creation;
                if(type == 0)
                    creation = Instantiate(onSurface.GetComponent<Item>().nextitem);
                else
                    creation = Instantiate(otherT.onSurface.GetComponent<Item>().nextitem);
                FinishCreate(creation);
            }
        }  // 1 = mango 2 = cabeza
        else if ((type == 1 && otherT.type == 2) || (type == 2 && otherT.type == 1)) {
            usable = true;
            correct = true;
            if (crafted) {
                GameObject creation;
                if (type == 2)
                    creation = Instantiate(onSurface.GetComponent<Item>().nextitem);
                else
                    creation = Instantiate(otherT.onSurface.GetComponent<Item>().nextitem);
                FinishCreate(creation);
            }
        } else {
            usable = false;
            correct = false;
            crafted = false;
            beingUsed = false;
        }
    }

    void FinishCreate(GameObject creation) {
        creation.transform.parent = this.transform;
        creation.transform.position = this.transform.position + (Vector3.up * offset);
        creation.transform.rotation = this.transform.rotation;
        GameObject.Destroy(onSurface);
        GameObject.Destroy(otherT.onSurface);
        onSurface = creation;
        otherT.onSurface = null;
        usable = false;
        correct = false;
        beingUsed = false;
        timer = 0;
        crafted = false;
        type = onSurface.GetComponent<Item>().type;

    }
}
