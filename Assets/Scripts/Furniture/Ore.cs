using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using cakeslice;

public class Ore : Surface
{
    public GameObject ore;

    public float timer = 0;
    public float miningTime = 2.0f;
    PlayerController pl;
    public GameObject bar;

    public override void Start() {
        base.Start();
        usable = true;
        occupied = true;
    }

    public override void Update() {
        base.Update();

        if (beingUsed) {
            bar.transform.gameObject.SetActive(true);
            bar.transform.GetChild(1).GetComponent<Image>().fillAmount = timer / miningTime;
            timer += Time.deltaTime;
            if (timer >= miningTime) {
                giveOre();
            }
        } else {
            timer = 0;
            bar.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
            bar.transform.gameObject.SetActive(false);
        }
    }

    public override void PickOnSurface(PlayerController player) {
        if(player != pl)
            pl = player;
        beingUsed = true;
        player.usingTable = true;
        player.freezing = true;
    }

    void giveOre() {
        GameObject o = Instantiate(ore);
        o.GetComponent<Rigidbody>().isKinematic = true;
        o.GetComponent<Collider>().enabled = false;
        pl.onHand = o;
        pl.onHand.transform.position = pl.holdItem.transform.position;
        pl.onHand.transform.rotation = pl.holdItem.transform.rotation;
        pl.onHand.transform.parent = pl.holdItem.transform;

        timer = 0;
        beingUsed = false;
        bar.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
        bar.transform.gameObject.SetActive(false);
        pl.freezing = false;
        pl.usingTable = false;
        pl = null;
    }
}
