using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cauldron : Surface
{
    public float timer = 0;
    public float coolingTime = 4.0f;
    public bool cooled = false;
    public GameObject bar;

    public override void Update()
    {
        base.Update();

        if (beingUsed) {
            bar.transform.gameObject.SetActive(true);
            bar.transform.GetChild(2).GetComponent<Image>().fillAmount = 1 - (timer / coolingTime);
            timer += Time.deltaTime;
            if (timer >= coolingTime) {
                cooled = true;
            }
        } else {
            timer = 0;
            bar.transform.GetChild(2).GetComponent<Image>().fillAmount = 1;
            bar.transform.gameObject.SetActive(false);
        }
    }

    public override void LeaveInSurface(GameObject item) {
        if (item.GetComponent<Item>().heating) {
            base.LeaveInSurface(item);
            beingUsed = true;
        }
    }

    public override void PickOnSurface(PlayerController player) {
        if (cooled) {
            onSurface.GetComponent<Item>().heating = false;
            base.PickOnSurface(player);
            bar.transform.GetChild(2).GetComponent<Image>().fillAmount = 1;
            bar.transform.gameObject.SetActive(false);
            cooled = false;
            beingUsed = false;
            timer = 0;
        }
    }
}
