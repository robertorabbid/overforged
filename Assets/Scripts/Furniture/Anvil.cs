using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Anvil : Surface
{
    public RingMenu ring;
    public GameObject creation = null;
    public List<GameObject> creations = new List<GameObject>();
    PlayerController pl;

    public float timer = 0;
    public float hammerTime = 2.0f;
    public GameObject bar;

    public override void Update() {
        base.Update();

        if (onSurface != null) {
            if(onSurface.GetComponent<Item>().anvil && creation != null) {
                usable = true;

                if(beingUsed) {
                    bar.transform.gameObject.SetActive(true);
                    bar.transform.GetChild(1).GetComponent<Image>().fillAmount = timer / hammerTime;
                    timer += Time.deltaTime;
                    if(timer >= hammerTime) {
                        TransformItem();
                    }
                }
            } else {
                usable = false;
                bar.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
                bar.transform.gameObject.SetActive(false);
                timer = 0;
            }
        } else {
            usable = false;
            bar.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
            bar.transform.gameObject.SetActive(false);
            timer = 0;
        }
    }

    public override void Special(PlayerController player) {
        if (!player.onRing) {
            ring.gameObject.SetActive(true);
            player.onRing = true;
            player.freezing = true;
            if(player != pl)
                pl = player;
        } else {
            ring.activateCake(this);
        }
    }

    public override void LeaveInSurface(GameObject item) {
        if (item.GetComponent<Item>().anvil)
            base.LeaveInSurface(item);
    }

    public override void PickOnSurface(PlayerController player) {
        if (onSurface != null) {
            if (/*!onSurface.GetComponent<Item>().anvil*/ !player.viewUse) {
                bar.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
                bar.transform.gameObject.SetActive(false);
                base.PickOnSurface(player);
            } else/* if (player.viewUse)*/ {
                if(player != pl)
                    pl = player;
                beingUsed = true;
                player.usingTable = true;
                player.freezing = true;
            }
        }
    }

    void TransformItem() {
        GameObject o = Instantiate(creation);
        o.GetComponentInChildren<Renderer>().material = onSurface.GetComponent<Item>().bucketMaterial;
        GameObject.Destroy(onSurface);
        o.GetComponent<Rigidbody>().isKinematic = false;
        o.GetComponent<Rigidbody>().Sleep();
        o.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        o.GetComponent<Collider>().enabled = true;
        if(o.transform.parent != null)
            o.transform.parent.parent.GetComponent<PlayerController>().onHand = null;
        o.transform.parent = this.gameObject.transform;
        o.transform.rotation = this.transform.rotation;
        o.transform.position = this.transform.position + (Vector3.up * offset);
        onSurface = o;
        occupied = true;

        timer = 0;
        bar.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
        bar.transform.gameObject.SetActive(false);
        beingUsed = false;
        pl.freezing = false;
        pl.usingTable = false;
        pl = null;
    }

    public void Ringed(string s) {
        pl.onRing = false;
        pl.freezing = false;
        pl = null;
        GameObject nextCreation;

        switch (s) {
            case "0":
                nextCreation = null;
                break;
            case "ring":
                nextCreation = creations[0];
                break;
            case "pickaxe":
                nextCreation = creations[1];
                break;
            case "axe":
                nextCreation = creations[2];
                break;
            default:
                nextCreation = null;
                break;
        }

        if (nextCreation != creation) {
            timer = 0;
            bar.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
        }
        creation = nextCreation;
    }
}
