using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : Surface
{
    public GameObject inBox;

    public override void PickOnSurface(PlayerController player) {
        if (!occupied) {
            GameObject boxed = Instantiate(inBox);
            boxed.GetComponent<Rigidbody>().isKinematic = true;
            boxed.GetComponent<Collider>().enabled = false;
            player.onHand = boxed;
            player.onHand.transform.position = player.holdItem.transform.position;
            player.onHand.transform.parent = player.holdItem.transform;
        } else
            base.PickOnSurface(player);
    }
}
