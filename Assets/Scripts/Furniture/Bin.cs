using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bin : Surface
{
    public override void LeaveInSurface(GameObject item) {
        item.GetComponent<Rigidbody>().isKinematic = false;
        item.GetComponent<Rigidbody>().Sleep();
        item.GetComponent<Collider>().enabled = true;
        item.transform.parent = this.gameObject.transform;
        item.transform.position = this.transform.position + (Vector3.up * offset);
        StartCoroutine(DeleteThis(item));
    }

    IEnumerator DeleteThis(GameObject item) {
        yield return new WaitForSeconds(0.5f);
        GameObject.Destroy(item);
    }
}
