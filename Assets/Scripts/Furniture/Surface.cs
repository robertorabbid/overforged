using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;

public class Surface : MonoBehaviour
{
    public bool selected = false;
    public bool occupied = false;

    public bool usable = false;
    public bool beingUsed = false;

    public GameObject onSurface = null;
    public float offset = 1.0f;

    [HideInInspector]
    public Outline outline;

    public virtual void Start() {
        outline = this.gameObject.GetComponentInChildren<Outline>();
    }

    public virtual void Update() {
        if(selected)
            outline.eraseRenderer = false;
        else
            outline.eraseRenderer = true;
    }
    public virtual void LeaveInSurface(GameObject item)
    {
        if (!occupied) {
            item.GetComponent<Rigidbody>().isKinematic = false;
            item.GetComponent<Rigidbody>().Sleep();
            item.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            item.GetComponent<Collider>().enabled = true;
            if (item.transform.parent != null)
                item.transform.parent.parent.GetComponent<PlayerController>().onHand = null;
            item.transform.parent = this.gameObject.transform;
            item.transform.rotation = this.transform.rotation;
            item.transform.position = this.transform.position + (Vector3.up * offset);
            onSurface = item;
            occupied = true;
        }
    }

    public virtual void PickOnSurface(PlayerController player) {
        if (occupied && onSurface.GetComponent<Item>().pickable) {
            onSurface.GetComponent<Rigidbody>().isKinematic = true;
            onSurface.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            onSurface.GetComponent<Collider>().enabled = false;
            player.onHand = onSurface;
            onSurface = null;
            occupied = false;
            player.onHand.transform.position = player.holdItem.transform.position;
            player.onHand.transform.rotation = player.holdItem.transform.rotation;
            player.onHand.transform.parent = player.holdItem.transform;
        }
    }

    public virtual void Special(PlayerController player) {

    }
}
