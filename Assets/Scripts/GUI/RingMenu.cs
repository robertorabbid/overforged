using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingMenu : MonoBehaviour
{
    public Ring data;
    public RingCakePiece ringCakePiecePrefab;
    public float gapWithDegree = 1f;
    public RingCakePiece[] pieces;
    protected RingMenu parent;


    PlayerController player;
    public int cakeSelected = 0;

    // Start is called before the first frame update
    void Start() {
        float stepLength = 360f / data.elements.Length;
        float iconDist = Vector3.Distance(ringCakePiecePrefab.icon.transform.position, ringCakePiecePrefab.cakePiece.transform.position);

        pieces = new RingCakePiece[data.elements.Length];

        for (int i = 0; i < data.elements.Length; i++) {
            pieces[i] = Instantiate(ringCakePiecePrefab, transform);
            pieces[i].transform.localPosition = Vector3.zero;
            pieces[i].transform.localRotation = Quaternion.identity;

            pieces[i].cakePiece.fillAmount = 1f / data.elements.Length - gapWithDegree / 360f;
            pieces[i].cakePiece.transform.localPosition = Vector3.zero;
            pieces[i].cakePiece.transform.localRotation = Quaternion.Euler(0, 0, -stepLength / 2f + gapWithDegree / 2f + (i+1) * stepLength);
            pieces[i].cakePiece.color = new Color(1f, 1f, 1f, 0.5f);

            pieces[i].icon.transform.localPosition = pieces[i].cakePiece.transform.localPosition + Quaternion.AngleAxis(i * stepLength, Vector3.forward) * Vector3.up * iconDist;
            pieces[i].icon.sprite = data.elements[i].icon;
        }
    }

    // Update is called once per frame
    void Update() {
        for (int i = 0; i < data.elements.Length; i++) {
            if (i == cakeSelected)
                pieces[i].cakePiece.color = new Color(1f, 1f, 1f, 0.75f);
            else
                pieces[i].cakePiece.color = new Color(1f, 1f, 1f, 0.5f);
        }
    }

    public void RightCake() {
        cakeSelected--;
        if(cakeSelected < 0) cakeSelected = data.elements.Length - 1;
    }

    public void LeftCake() {
        cakeSelected++;
        if(cakeSelected >= data.elements.Length) cakeSelected = 0;
    }

    public void activateCake(Anvil anvil) {
        string p = data.elements[cakeSelected].name;

        anvil.Ringed(p);
        cakeSelected = 0;
        gameObject.SetActive(false);
    }

    public void getGamepadAxis(Vector2 axis) {
        float stepLength = 360f / data.elements.Length;
        float angle = NormalizeAngle(Vector3.SignedAngle(Vector3.up, new Vector3(axis.x, axis.y, 0), Vector3.forward) + stepLength / 2f);
        cakeSelected = (int)(angle / stepLength);
    }

    float NormalizeAngle(float a) => (a + 360f) % 360f;
}
